﻿using Microsoft.Analytics.Interfaces;
using Microsoft.Analytics.Interfaces.Streaming;
using Microsoft.Analytics.Types.Sql;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;


/// <summary>
///    Writing user-defined functions in a C# assembly and referencing them in the U-SQL script is preferred for more complex functions 
///    if the logic of the function requires the full power of C# beyond its expression language,
///    such as procedural logic or recursion.
/// </summary>
namespace BasicFunctions
{
    public static class Helpers
    {
        public static string HasPassed(int maxPoints, int pointsEarned)
        {            
            return ((float)pointsEarned / (float)maxPoints) > 0.6 ? "Passed" : "Failed"; ;
        }
    }
    
}
