﻿using Microsoft.Analytics.Interfaces;
using Microsoft.Analytics.Interfaces.Streaming;
using Microsoft.Analytics.Types.Sql;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace WindowFunctions
{
    public static class Helpers
    {
        public static string IsBetterThanOverallAverage(double? overallAverage, double? yearlyAverage )
        {
            return (yearlyAverage > overallAverage) ? "Better" : "Worse";
        }
    }
}
